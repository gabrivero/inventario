import os
import xmlrpclib
import sys
import csv
import math
from datetime import datetime


# ... define HOST, PORT, DB, USER, PASS
#HOST='186.153.139.146'
HOST = 'localhost'
PORT=8169
DB='sigren7_03_08'
USER='admin'
PASS='4zgUdOXafUZ2pwXhTKX5'

url = 'http://%s:%d/xmlrpc/common' % (HOST,PORT)
print url
sock = xmlrpclib.ServerProxy(url)
uid = sock.login(DB,USER,PASS)
print "Logged in as %s (uid:%d)" % (USER,uid)
# Create a new session
url = 'http://%s:%d/xmlrpc/object' % (HOST,PORT)
sock = xmlrpclib.ServerProxy(url)
print sock

csv_file = csv.reader(open('Bajas_agosto_FINAL.csv', 'rb'), delimiter=';')
cont = 0

outerr = ''
outerr += 'Log\n'
for line in csv_file:

    if cont > 0 :
        try:
            stock =  int(line[2])
        except:
            stock = 1
        # cont=cont+1
        # continue
        dni = line[9][::-1]
        dni = dni[:3] +  '.' + dni[3:]
        dni = dni[:7] +  '.' + dni[7:]
        dni = dni[::-1]

        stock_location_id = False

        employee_id = sock.execute(DB,uid, PASS, 'hr.employee', 'search', [('licenseno', '=', dni)])
        if not employee_id:
            outerr += 'linea %d:no se encuentra agente para el dni %s, nombre: %s \n'%(cont, dni, line[8])
            #print 
            outerr += '\tse utilizo id 1 por defecto \n'
            employee_id = 1
        else:
            employee_id=employee_id[0]
            stock_location_id = sock.execute(DB,uid, PASS, 'stock.location', 'search', [('agente_id', '=', employee_id),('usage','=','internal')])
        
        if not stock_location_id:
            outerr += 'linea %d:no se encuentra stock location para el agente %s, nombre: %s \n'%(cont, employee_id, line[8])
            stock_location_id = 28
            outerr += '\tSe utilo id 28 por defecto \n'
        else:
            if type(stock_location_id)!=int:
                stock_location_id=stock_location_id[0]
        data = {
        'prefix': line[0],
        'name': line[1],
        'stock_available': stock,
        'product_id': 1813,
        'date': line[4],
        'acta_recepcion': line[5],
        'destino_del_bien': line[7],
        'subresponsable_id': stock_location_id,
        'condicion_del_bien': line[12].lower(),
        'fecha_baja': line[10] and line[10] or False,
        'instrumento_legal': line[11],
        }

        lot_id = sock.execute(DB,uid,PASS,'stock.production.lot','create',data)

        data_stock = {
            'origin': line[5],
            'product_id': 1813,
            'product_qty': stock,
            'product_uom': 1,
            'price_unit': line[6],
            'date': line[4]+' 00:00:00',
            'prodlot_id': lot_id,
            'location_id':8,
            'location_dest_id':stock_location_id,
            'state': 'done',
            'descripcion': line[3],
            'name': line[3],
        }
        #print data_stock
        move_id = sock.execute(DB,uid,PASS,'stock.move','create',data_stock)
    cont=cont+1

print outerr